import os
import sys
import logging
import records
import numpy as np
import pandas as pd
import time
import pymysql
pymysql.install_as_MySQLdb()

# LOGGING
logging.basicConfig(format='%(levelname)s:%(message)s', level=logging.INFO)

# CONFIG
CLOUDSQL_USER = os.getenv('CLOUDSQL_USER')
CLOUDSQL_PASSWORD = os.getenv('CLOUDSQL_PASSWORD')
CLOUDSQL_DATABASE = os.getenv('CLOUDSQL_DATABASE')
connection_string = 'mysql://'+str(CLOUDSQL_USER)+':'+str(CLOUDSQL_PASSWORD)+'@127.0.0.1:3307/'+str(CLOUDSQL_DATABASE)


def pull_data():
    """Test connections, pull the data and transform the data"""
    attempts = 0
    while attempts < 5:
        try:
            db = records.Database(connection_string)
            break
        except Exception as e:
            attempts += 1
            logging.error('MySQL connection failed at attempt '+str(attempts)+' of 5')
            logging.error(e)
            time.sleep(5)
    try:
        db
        logging.info('MySQL connection successful')
    except NameError:
        logging.error('Could not connect to MySQL after 5 attempts')
        sys.exit()

    # Query class tag to create a studio and class tag matrix
    class_tag = pd.read_sql(open(os.path.join('MYSQL', 'class_tag.sql')).read(),
        connection_string)

    # Query studio data and return active studios
    studio = pd.read_sql(open(os.path.join('MYSQL', 'studio.sql')).read(),
        connection_string)

    # Query transaction data and return a user_id by studio_id matrix
    studio_activity = pd.read_sql(open(os.path.join('MYSQL', 'studio_activity.sql')).read(),
        connection_string)

    # Query user data and return all users who have an active enrollment
    # and Peerfit account
    user = pd.read_sql(open(os.path.join('MYSQL', 'user.sql')).read(),
        connection_string)

    # Query specific user ids for JOCO and Peerfit
    specific_users = pd.read_sql(open(os.path.join('MYSQL', 'specific_user_ids.sql')).read(),
        connection_string)

    # Query user workout preference data
    workout_pref = pd.read_sql(open(os.path.join('MYSQL', 'workout_preference.sql')).read(),
        connection_string)

    return class_tag, studio, studio_activity, user, workout_pref, specific_users
