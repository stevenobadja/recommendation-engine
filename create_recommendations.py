import pandas as pd
import pull_data as pa
import barebones as bb
import xlsxwriter

# pull & transform data
class_tag, studio, studio_activity, user, workout_pref, specific_users = pa.pull_data()
user_list, user_studio_rating = bb.get_final_user_list_and_user_studio_rating(studio_activity, min_rating = 2)
user_studio_matrix = bb.get_matrix(studio_activity[studio_activity.user_id.isin(user_list)].reset_index(drop=True),
                                   row='user_id',
                                   col='studio_id')

# 613 - Andre O., 23694 - Stu R., 15015 - Steven O., 1182 - Ed B., 1156 - Emma M.
# 12303 - Lisa Z., 16309 - Ben O., 24708 - Mat S., 848 - Christina V.
# 3900 - Gaby M., 13451 - Saif K., 7433 - Dennis E., 23695 - Amanda K.
su, ss, susm, ls = bb.get_recommendation(studio_activity,
                                         user_studio_matrix,
                                         user_studio_rating,
                                         sim_thresh=0,
                                         min_rating=2,
                                         subject_user=23695)

# Obtain labels for results
su = bb.get_labels(su, user, key='user_id')
ss = bb.get_labels(ss, studio, key='studio_id')
ls = bb.get_labels(ls, studio, key='studio_id')

# Create a Pandas Excel writer using XlsxWriter as the engine.
writer = pd.ExcelWriter(su.first_name[0]+'_'+su.last_name[0]+'_summary.xlsx', engine='xlsxwriter')

# Create a sheets
su.to_excel(writer, sheet_name='Similar Users', index=False)
ss.to_excel(writer, sheet_name='Studio Recommendations', index=False)
susm.to_excel(writer, sheet_name='User Studio Matrix')
ls.to_excel(writer, sheet_name='Liked Studios', index=False)

# Close the Pandas Excel writer and output the Excel file.
writer.save()
