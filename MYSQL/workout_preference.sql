/*
Pull all tags users workout preference

As of date: 02/03/20
*/

SELECT CURDATE()    AS today,
       ti.object_id AS user_id,
       ti.tag_id,
       pt.name      AS tag_name
FROM   peerfit.tagsplus_taggeditem AS ti
       LEFT JOIN peerfit.tagsplus_tag AS pt
              ON ti.tag_id = pt.id
WHERE  ti.content_type_id = 110
