/*
Pull all users who has an active CORE Peerfit account
Exclue MOVE users (where sponsor_id = 19147 or 19426)

As of date: 02/03/20
*/
SELECT CURDATE()                   AS `today`,
       users_user.id               AS `user_id`,
       users_user.first_name       AS `first_name`,
       users_user.last_name        AS `last_name`,
       DATE(users_user.birth_date) AS `birth_date`,
       users_user.gender           AS `gender`,
       sponsors_enrollment.zipcode AS `zip`
FROM   peerfit.users_user AS users_user
       LEFT JOIN peerfit.sponsors_sponsored_user_new AS
                 sponsors_sponsored_user_new
              ON users_user.id = sponsors_sponsored_user_new.user_id
                 AND sponsors_sponsored_user_new.invalidated_on IS NULL
                 AND sponsors_sponsored_user_new.validated_on IS NOT NULL
       LEFT JOIN peerfit.sponsors_enrollment AS sponsors_enrollment
              ON sponsors_sponsored_user_new.enrollment_key =
                 sponsors_enrollment.enrollment_key
WHERE  users_user.id IN (SELECT DISTINCT transactions_transaction.user_id
                         FROM   peerfit.transactions_transaction AS
                                transactions_transaction
                         WHERE  transactions_transaction.transaction_type_id IN
                                ( 1, 5,
                                11, 23 )
                                AND transactions_transaction.valid_from >=
                                    CURDATE() - INTERVAL 365 day)
       AND sponsors_sponsored_user_new.sponsor_id NOT IN ( 19147, 19426 )
       AND sponsors_enrollment.is_active
GROUP  BY 1,
          2,
          3,
          4,
          5,
          6,
          7
