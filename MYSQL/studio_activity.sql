/*
Pull studio class taken, membership reservation taken, gym check-in's within the last 365 days
and only include studios that are still current

*****NEED TO AUTO REMOVE TEST OR DEMO LOCATIONS
current demo test sites listed below and is excluded

Reservation Type:
1 = Membership Visit
2 = Membership Class
3 = Studio Class

Status:
3 = Confirmed
7 - Verified by user
8 = Verified by studio

As of date: 11/21/19
*/
SELECT CURDATE()                                           AS `today`,
       sweatband_reservation.id                            AS `reservation_id`,
       DATE(sweatband_reservation.class_starts_at)         AS `class_date`,
       sweatband_reservation.user_id                       AS `user_id`,
       sweatband_reservation.studio_id                     AS `studio_id`,
       COALESCE(sweatband_reservation.studio_class_id, -1) AS `class_id`
FROM   peerfit.sweatband_reservation AS sweatband_reservation
WHERE  (sweatband_reservation.reservation_type IN (1, 2, 3))
       AND (sweatband_reservation.status IN (3, 7, 8))
       AND (DATE(sweatband_reservation.class_starts_at) BETWEEN
                 CURDATE() - INTERVAL 365 day AND CURDATE())
       AND (sweatband_reservation.studio_id IN (
				 		SELECT sweatband_studio.id AS `studio_id`
	          FROM peerfit.sweatband_studio AS sweatband_studio
            WHERE sweatband_studio.deleted_at IS NULL
              AND sweatband_studio.is_displayed = 1))
       AND sweatband_reservation.studio_id NOT IN (15108, 14846, 15978, 15979,
                                                   15446, 19350, 15442, 16569,
                                                   16571, 19855, 8754, 11377,
                                                   11379, 11378, 16384, 16385,
                                                   19327, 6213, 659, 16374,
                                                   19187, 6650, 8848, 718, 229,
                                                   685, 691, 231, 7092, 1140,
                                                   9904, 1231, 1186, 6873, 313,
                                                   1223, 1537, 1509, 19778,
                                                   1521, 19733, 952, 932, 1159,
                                                   627, 6196, 913, 1218, 4392,
                                                   1798, 19781, 314, 535, 787,
                                                   652, 16383, 16387, 1552,
                                                   12938, 8721, 5066)
GROUP  BY 1,
          2,
          3,
          4,
          5,
          6
