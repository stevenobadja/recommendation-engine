/*
Pull studio class taken, membership reservation taken, gym check-in's within the last 365 days
and only include studios that are still current

*****NEED TO AUTO REMOVE TEST OR DEMO LOCATIONS
current demo test sites listed below and is excluded

Reservation Type:
1 = Membership Visit
2 = Membership Class
3 = Studio Class

Status:
3 = Confirmed
7 - Verified by user
8 = Verified by studio

As of date: 02/03/20
*/

SELECT CURDATE()                                           AS `today`,
       sweatband_reservation.id                            AS `reservation_id`,
       DATE(sweatband_reservation.class_starts_at)         AS `class_date`,
       sweatband_reservation.user_id                       AS `user_id`,
       sweatband_reservation.studio_id                     AS `studio_id`,
       COALESCE(sweatband_reservation.studio_class_id, -1) AS `class_id`
FROM   peerfit.sweatband_reservation AS sweatband_reservation
WHERE  (sweatband_reservation.reservation_type IN (1, 2, 3))
       AND (sweatband_reservation.status IN (3, 7, 8))
       AND (DATE(sweatband_reservation.class_starts_at) BETWEEN
                 CURDATE() - INTERVAL 365 day AND CURDATE())
       AND (sweatband_reservation.studio_id IN (
              SELECT sweatband_studio.id AS `studio_id`
              FROM peerfit.sweatband_studio AS sweatband_studio
              WHERE sweatband_studio.is_displayed
              AND (NOT COALESCE(sweatband_studio.is_deleted , FALSE))
              AND  (( ( CASE
                       WHEN sweatband_studio.name_override IS NULL THEN
                       LOWER(sweatband_studio.external_name)
                       WHEN sweatband_studio.name_override = '' THEN
                       LOWER(sweatband_studio.external_name)
                       ELSE LOWER(sweatband_studio.name_override)
                     end ) NOT LIKE '% test % '
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE 'test %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% test'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% peerfit %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE 'peerfit %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% peerfit'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% demo %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE 'demo %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% demo'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% site %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE 'site %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% site'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% affiliate %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE 'affiliate %'
                    OR ( CASE
                           WHEN sweatband_studio.name_override IS NULL THEN
                           LOWER(sweatband_studio.external_name)
                           WHEN sweatband_studio.name_override = '' THEN
                           LOWER(sweatband_studio.external_name)
                           ELSE LOWER(sweatband_studio.name_override)
                         end ) NOT LIKE '% affiliate'
                    OR CASE
                         WHEN sweatband_studio.name_override IS NULL THEN
                         LOWER(sweatband_studio.external_name)
                         WHEN sweatband_studio.name_override = '' THEN
                         LOWER(sweatband_studio.external_name)
                         ELSE LOWER(sweatband_studio.name_override)
                       end IN ( 'test', 'testest', 'testfuture', 'testing studio' ) ))

              ))
GROUP  BY 1,
          2,
          3,
          4,
          5,
          6
