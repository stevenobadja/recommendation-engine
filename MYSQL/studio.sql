/*
Pull active studios only

Booking Service:
10 = Non-integrated Studio
20 - Mindbody booking service
30 = Affiliate booking service
40 = Zendesk booking service

As of date: 02/03/20
*/
SELECT CURDATE()                    AS `today`,
       sweatband_studio.id          AS `studio_id`,
       CASE
         WHEN sweatband_studio.name_override IS NULL THEN sweatband_studio.external_name
         WHEN sweatband_studio.name_override = '' THEN sweatband_studio.external_name
         ELSE sweatband_studio.name_override
       END                          AS `studio_name`,
       CASE
         WHEN sweatband_studio.address_override IS NULL THEN REPLACE(JSON_EXTRACT(sweatband_studio.external_address, '$.line1'), '"', '')
         ELSE REPLACE(JSON_EXTRACT(sweatband_studio.address_override, '$.line1'), '"', '')
       END                          AS `address`,
       CASE
         WHEN sweatband_studio.address_override IS NULL THEN REPLACE(JSON_EXTRACT(sweatband_studio.external_address, '$.city_name'), '"', '')
         ELSE REPLACE(JSON_EXTRACT(sweatband_studio.address_override, '$.city_name'), '"', '')
       END                          AS `city`,
       CASE
         WHEN sweatband_studio.address_override IS NULL THEN REPLACE(JSON_EXTRACT(sweatband_studio.external_address, '$.subdivision_code'), '"', '')
         ELSE REPLACE(JSON_EXTRACT(sweatband_studio.address_override, '$.subdivision_code'), '"', '')
       END                          AS `state`,
       CASE
         WHEN sweatband_studio.address_override IS NULL THEN REPLACE(JSON_EXTRACT(sweatband_studio.external_address, '$.postal_code'), '"', '')
         ELSE REPLACE(JSON_EXTRACT(sweatband_studio.address_override, '$.postal_code'), '"', '')
       END                          AS `zip`,
       ST_Y(sweatband_studio.point) AS `lat`,
       ST_X(sweatband_studio.point) AS `lng`,
       banner_image AS `banner`
FROM   peerfit.sweatband_studio AS sweatband_studio
WHERE  sweatband_studio.is_displayed
  AND (NOT COALESCE(sweatband_studio.is_deleted , FALSE))
GROUP  BY 1,
          2,
          3,
          4,
          5,
          6,
          7,
          8,
          9,
          10
