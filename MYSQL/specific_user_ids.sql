/*
Pull all user ids for users who belong to:
1. Johnson County - 181
2. Peerfit - 115
3. PEERFIT INC - 16803

As of date: 02/03/20
*/
SELECT users_user.id                            AS `user_id`,
       sponsors_sponsored_user_new.sponsor_id
FROM   peerfit.users_user AS users_user
       LEFT JOIN peerfit.sponsors_sponsored_user_new AS
                 sponsors_sponsored_user_new
              ON users_user.id = sponsors_sponsored_user_new.user_id
                 AND sponsors_sponsored_user_new.invalidated_on IS NULL
                 AND sponsors_sponsored_user_new.validated_on IS NOT NULL
       LEFT JOIN peerfit.sponsors_enrollment AS sponsors_enrollment
              ON sponsors_sponsored_user_new.enrollment_key =
                 sponsors_enrollment.enrollment_key
WHERE  sponsors_sponsored_user_new.sponsor_id IN ( 181, 115, 16803 )
GROUP  BY 1,
          2
