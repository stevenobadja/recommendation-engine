/*
Pull all tags for a studio

As of date: 02/03/20
*/
SELECT CURDATE()                AS `today`,
       sweatband_studio.id      AS `studio_id`,
       sweatband_studioclass.id AS `class_id`,
       tagsplus_tag.name        AS `tag_name`
FROM peerfit.sweatband_studio AS sweatband_studio
    INNER JOIN peerfit.sweatband_studioclass AS sweatband_studioclass
            ON sweatband_studioclass.studio_id = sweatband_studio.id
    INNER JOIN peerfit.tagsplus_taggeditem AS tagsplus_taggeditem
            ON tagsplus_taggeditem.object_id = sweatband_studioclass.id
           AND tagsplus_taggeditem.content_type_id = 238
    INNER JOIN peerfit.tagsplus_tag AS tagsplus_tag
            ON tagsplus_tag.id = tagsplus_taggeditem.tag_id

WHERE sweatband_studio.is_displayed
  AND (NOT COALESCE(sweatband_studio.is_deleted , FALSE))
GROUP BY 1,
         2,
         3,
         4
