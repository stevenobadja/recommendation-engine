import numpy as np
import pandas as pd
from scipy.stats.stats import spearmanr as spearman_r


def get_final_user_list_and_user_studio_rating(df, min_rating = 2):
    """
    From the studio activity log table, return studio activity with only
    """
    df_grouped = df.groupby(['user_id', 'studio_id']).size().reset_index(name='rating')
    df_grouped_min = df_grouped[df_grouped.rating >= min_rating].reset_index(drop=True)

    # Unique user list
    user_list = list(set(df_grouped_min.user_id))

    # User studio rating table
    df_grouped = df_grouped[df_grouped.user_id.isin(user_list)].reset_index(drop=True)
    return user_list, df_grouped


def get_matrix(df, row='user_id', col='studio_id'):
    """
    Transform studio activity log table into a user by studio matrix.
    """
    user_studio_matrix = pd.crosstab(index = df[row],
                                     columns = df[col])
    user_studio_matrix.columns = list(user_studio_matrix)
    # del user_studio_matrix.index.name
    return user_studio_matrix


def get_similarity(M, subject, similarity_threshold, label='user_id'):
    """
    Given a number of similar users and subject user,
    return similar users that has a positive correlation score
    """
    sim=[]
    sim = [(s, spearman_r(M.loc[subject], M.loc[s])[0]) for s in M.index]
    sim.sort(key= lambda tup: tup[1], reverse=True)
    sim_df = pd.DataFrame(sim, columns=[label, 'sim_score'])
    sim_df = sim_df[sim_df['sim_score']>similarity_threshold].reset_index(drop=True)
    return sim_df


def get_recommendation(studio_activity_df, user_studio_matrix, user_studio_rating, sim_thresh=0, min_rating=2, subject_user=15015):
    """
    Given our studio activity: matrix and rating, return the subject user
    recommendations from their liked studio history and exclude any studios
    that the user has visited
    """
    # Obtain subject user's similar user list
    similar_users = get_similarity(user_studio_matrix,
                                   subject_user,
                                   sim_thresh,
                                   label='user_id')

    # Obtain subject user's like studios list
    liked_studios = get_liked_studios_list(subject_user,
                                           user_studio_rating,
                                           min_rating)

    # Create user by studio matrix specific to subject user
    subject_studio_activity = studio_activity_df[studio_activity_df.user_id.isin(similar_users.user_id)].reset_index(drop=True)
    subject_user_studio_matrix = get_matrix(subject_studio_activity,
                                            row='studio_id',
                                            col='user_id')

    # Obtain user's visit user_history
    user_history = subject_studio_activity[subject_studio_activity.user_id == subject_user].reset_index(drop=True)

    # Obtain similar studios to the subject user's liked studios
    similar_studios = pd.DataFrame()
    for ls in liked_studios.studio_id:
        similar_studios = similar_studios.append(get_similarity(subject_user_studio_matrix,
                                                                ls,
                                                                sim_thresh,
                                                                label='studio_id'),
                                                sort=False
                                                )
    similar_studios = similar_studios[~similar_studios.studio_id.isin(liked_studios)]
    similar_studios = similar_studios[~similar_studios.studio_id.isin(user_history.studio_id)]
    similar_studios = similar_studios.groupby('studio_id')['sim_score'].max().reset_index(name='sim_score')
    similar_studios = similar_studios.sort_values(by='sim_score', ascending=False).reset_index(drop=True)

    return similar_users, similar_studios, subject_user_studio_matrix, liked_studios


def get_liked_studios_list(userid, data_ref, min_rating):
    """
    Return list of studios of user that has met the minimum rating
    """
    liked_studios = data_ref[(data_ref.user_id == userid) & (data_ref.rating >= min_rating)].reset_index(drop=True)
    return liked_studios


def get_labels(df, ref_df, key='user_id'):
    """
    Obtain labels
    """
    return pd.merge(df, ref_df, how='left', on=key)
